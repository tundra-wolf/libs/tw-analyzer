use std::path::PathBuf;

use tw_errors::{AnalysisError, TWError};

/// Create a source file for a Makefile, or return an error
pub fn create_makefile_source(makefile: PathBuf) -> Result<SourceFile, TWError> {
    let file = SourceFile::from(makefile);
    if file.language != SourceLanguage::Make {
        Err(TWError::AnalyzerError(AnalysisError::NotAMakefile(file.file.clone())))
    } else {
        Ok(file)
    }
}

/// Create a source file for a shell script, or return an error
pub fn create_shell_source(script: PathBuf) -> Result<SourceFile, TWError> {
    let file = SourceFile::from(script);
    if file.language != SourceLanguage::Shell {
        Err(TWError::AnalyzerError(AnalysisError::NotAShellScript(
            file.file.clone(),
        )))
    } else {
        Ok(file)
    }
}

/// File
#[derive(Debug)]
pub struct SourceFile {
    pub file: PathBuf,
    pub language: SourceLanguage,
}

impl SourceFile {
    pub fn new(file: PathBuf, language: SourceLanguage) -> SourceFile {
        SourceFile { file, language }
    }

    pub fn set_file(&mut self, file: PathBuf) {
        self.file = file;
    }

    pub fn set_language(&mut self, language: SourceLanguage) {
        self.language = language;
    }
}

impl From<PathBuf> for SourceFile {
    fn from(value: PathBuf) -> Self {
        let mut language = SourceLanguage::Unknown;

        // Based on full file name
        language = if let Some(file_name) = value.file_name() {
            if let Some(file_name) = file_name.to_str() {
                SourceLanguage::from(file_name)
            } else {
                language
            }
        } else {
            language
        };

        // Find type based on the extension
        language = if language == SourceLanguage::Unknown {
            if let Some(extension) = value.extension() {
                if let Some(extension) = extension.to_str() {
                    SourceLanguage::from(extension)
                } else {
                    language
                }
            } else {
                language
            }
        } else {
            language
        };

        SourceFile::new(value, language)
    }
}

impl From<&str> for SourceFile {
    fn from(value: &str) -> Self {
        let path = PathBuf::from(value);

        SourceFile::from(path)
    }
}

/// Language
#[derive(Debug, PartialEq)]
pub enum SourceLanguage {
    AsciiDoc,
    AspNet,
    Binary,
    Bson,
    C,
    CMake,
    Csharp,
    Css,
    Cxx,
    DosBatch,
    Html,
    Ini,
    Javascript,
    Json,
    Make,
    Markdown,
    Powershell,
    Rust,
    Shell,
    Sql,
    Text,
    Toml,
    Unknown,
    VisualBasic,
    Xml,
}

impl From<&str> for SourceLanguage {
    fn from(value: &str) -> Self {
        // Full file names
        let mut language = match value {
            "a.out" => Self::Binary,
            "CMakeLists.txt" => Self::CMake,
            "BSDmakefile" | "GNUmakefile" | "Makefile" | "makefile" => Self::Make,
            "configure" => Self::Shell,
            _ => Self::Unknown,
        };

        if language == Self::Unknown {
            // Extension only
            language = match value.to_lowercase().as_str() {
                "adoc" => Self::AsciiDoc,
                "asp" | "aspx" => Self::AspNet,
                "a" | "dll" | "o" | "obj" | "so" => Self::Binary,
                "bson" => Self::Bson,
                "c" | "h" | "i" => Self::C,
                "cmake" => Self::CMake,
                "cs" => Self::Csharp,
                "css" => Self::Css,
                "cc" | "c++" | "cxx" | "hh" | "hpp" => Self::Cxx,
                "bat" => Self::DosBatch,
                "htm" | "html" => Self::Html,
                "ini" => Self::Ini,
                "js" => Self::Javascript,
                "json" => Self::Json,
                "make" | "mk" => Self::Make,
                "md" => Self::Markdown,
                "ps1" => Self::Powershell,
                "rs" => Self::Rust,
                "csh" | "ksh" | "sh" => Self::Shell,
                "sql" => Self::Sql,
                "txt" => Self::Text,
                "toml" => Self::Toml,
                "vb" => Self::VisualBasic,
                "xml" => Self::Xml,
                _ => Self::Unknown,
            }
        }

        language
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn source_file() {
        let file = SourceFile::new(PathBuf::from("test.sql"), SourceLanguage::Sql);
        assert_eq!(file.file, PathBuf::from("test.sql"));
        assert_eq!(file.language, SourceLanguage::Sql);
    }

    #[test]
    fn source_file_from() {
        let file = SourceFile::from("test.sql");
        assert_eq!(file.file, PathBuf::from("test.sql"));
        assert_eq!(file.language, SourceLanguage::Sql);

        let file = SourceFile::from(PathBuf::from("./tundra-wolf/Cargo.TOML"));
        assert_eq!(file.file, PathBuf::from("./tundra-wolf/Cargo.TOML"));
        assert_eq!(file.language, SourceLanguage::Toml);

        let file = SourceFile::from(PathBuf::from("./Makefile"));
        assert_eq!(file.file, PathBuf::from("./Makefile"));
        assert_eq!(file.language, SourceLanguage::Make);

        let file = SourceFile::from(PathBuf::from("/home/rsergeant/project/Main.C"));
        assert_eq!(file.file, PathBuf::from("/home/rsergeant/project/Main.C"));
        assert_eq!(file.language, SourceLanguage::C);

        let file = SourceFile::from(PathBuf::from("C:\test\test.abc"));
        assert_eq!(file.file, PathBuf::from("C:\test\test.abc"));
        assert_eq!(file.language, SourceLanguage::Unknown);
    }

    #[test]
    fn source_language() {
        let languages = vec![
            SourceLanguage::AsciiDoc,
            SourceLanguage::AspNet,
            SourceLanguage::Binary,
            SourceLanguage::Bson,
            SourceLanguage::C,
            SourceLanguage::CMake,
            SourceLanguage::Csharp,
            SourceLanguage::Css,
            SourceLanguage::Cxx,
            SourceLanguage::DosBatch,
            SourceLanguage::Html,
            SourceLanguage::Ini,
            SourceLanguage::Javascript,
            SourceLanguage::Json,
            SourceLanguage::Make,
            SourceLanguage::Markdown,
            SourceLanguage::Powershell,
            SourceLanguage::Rust,
            SourceLanguage::Shell,
            SourceLanguage::Sql,
            SourceLanguage::Text,
            SourceLanguage::Toml,
            SourceLanguage::Unknown,
            SourceLanguage::VisualBasic,
            SourceLanguage::Xml,
        ];

        for language in languages {
            assert!(match language {
                SourceLanguage::AsciiDoc => true,
                SourceLanguage::AspNet => true,
                SourceLanguage::Binary => true,
                SourceLanguage::Bson => true,
                SourceLanguage::C => true,
                SourceLanguage::CMake => true,
                SourceLanguage::Csharp => true,
                SourceLanguage::Css => true,
                SourceLanguage::Cxx => true,
                SourceLanguage::DosBatch => true,
                SourceLanguage::Html => true,
                SourceLanguage::Ini => true,
                SourceLanguage::Javascript => true,
                SourceLanguage::Json => true,
                SourceLanguage::Make => true,
                SourceLanguage::Markdown => true,
                SourceLanguage::Powershell => true,
                SourceLanguage::Rust => true,
                SourceLanguage::Shell => true,
                SourceLanguage::Sql => true,
                SourceLanguage::Text => true,
                SourceLanguage::Toml => true,
                SourceLanguage::Unknown => true,
                SourceLanguage::VisualBasic => true,
                SourceLanguage::Xml => true,
            })
        }
    }

    #[test]
    fn source_language_from() {
        // Full file names
        assert_eq!(SourceLanguage::Binary, SourceLanguage::from("a.out"));
        assert_eq!(SourceLanguage::CMake, SourceLanguage::from("CMakeLists.txt"));
        assert_eq!(SourceLanguage::Make, SourceLanguage::from("BSDmakefile"));
        assert_eq!(SourceLanguage::Shell, SourceLanguage::from("configure"));

        // Extensions
        assert_eq!(SourceLanguage::AsciiDoc, SourceLanguage::from("adoc"));
        assert_eq!(SourceLanguage::AspNet, SourceLanguage::from("ASP"));
        assert_eq!(SourceLanguage::AspNet, SourceLanguage::from("ASPx"));
        assert_eq!(SourceLanguage::Binary, SourceLanguage::from("a"));
        assert_eq!(SourceLanguage::Bson, SourceLanguage::from("BSon"));
        assert_eq!(SourceLanguage::C, SourceLanguage::from("C"));
        assert_eq!(SourceLanguage::CMake, SourceLanguage::from("cmake"));
    }
}
