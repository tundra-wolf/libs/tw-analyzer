use std::fmt::Debug;

use tw_errors::TWError;

/// Analyzer trait
pub trait Analyzer {
    fn analyse(&mut self) -> Result<Box<dyn AnalysisResult>, TWError>;
}

/// Analysis result trait
pub trait AnalysisResult: Debug {}
