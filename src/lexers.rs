use std::fmt::Debug;

/// Lexer trait
pub trait Lexer {
    fn get_token(&mut self, next: bool) -> Option<Box<dyn Token>>;
}

/// Token trait
pub trait Token: Debug {}
