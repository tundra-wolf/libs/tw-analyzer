use std::fmt::Debug;

use tw_errors::TWError;
use tw_source::SourceId;

/// Preprocess trait implemented by all preprocessors. A preprocessor in this case sits
/// between source code reader and the programming language lexer.
pub trait Preprocess<Settings> {
    fn new(id: SourceId, settings: &Settings) -> Result<Self, TWError>
    where
        Self: Sized;
    fn preprocess(&mut self) -> Result<Box<dyn PreprocessorResult>, TWError>;
}

/// Preprocess result
pub trait PreprocessorResult: Debug {}
