use std::fmt::Debug;

use tw_errors::TWError;

/// Parser trait
pub trait Parser {
    fn parse(&mut self) -> Result<Box<dyn ParseResult>, TWError>;
}

/// Parse result trait
pub trait ParseResult: Debug {}
