# README for the Analyzer Library (package `tw-analyzer`)

This library contains common components for the analyzer plugins. These components are divived into 2 main modules. The `analyzers` module contains components for semantic analysis. The `parsers` module contains parsers and syntactic analysis components.

## Analyzers

## Parsers
